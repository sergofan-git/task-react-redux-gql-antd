import React, { Component } from 'react';
import AppLayout from './AppLayout'

class App extends Component {
  render() {
    return (
      <AppLayout />
    );
  }
}

export default App;
